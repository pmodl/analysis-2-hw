\documentclass{article}
\usepackage[margin=1in]{geometry}
\input{include.d/macros}

\title{Analysis II}
\author{ Paul Modlin }
\date{\today}

\begin{document}
\begin{thm}[5.1]
	If $\{h_n\}$ is a sequence of nonnegative integrable functions on $E$
	with $\{h_n\}\rightarrow 0$ almost everywhere in $E$.
	Then $\lim \int h_n = 0\iff \{h_n\}$ is uniformly integrable and tight over $E$.
\end{thm}
\begin{proof}
	Let $\{h_n\}$ is a sequence of nonnegative integrable functions on $E$
	with $\{h_n\}\rightarrow 0$\ae~on $E$.

	If $f$ is uniformly integral and tight,
	then $\lim_{n\rightarrow\infty}\int_E h_n=0$
	by the Vitali Convergence Theorem.

	Let $\varepsilon>0$ be given.
	If $\lim_{n\rightarrow\infty}\int_E h_n = 0$,
	then for for some index $M$, for all $n>M$, $\int_E |h_n| < \varepsilon$.
	Thus $h_n$ is tight.
	Additionally, for each $k\leq M$, there is some $\delta_k>0$ such that
	for all $A\subseteq E$ with $m(A)<\varepsilon$, $\int_A H_k<\varepsilon$.
	Now $h_n$ is uniformly integral with $\delta = \min\{\delta_1,\delta_2,\ldots,\delta_{M-1}\}$.
\end{proof}


\begin{thm}[5.2]
	All finite integrable families are uniformly integrable and tight.
\end{thm}
\begin{proof}[Sketch]
	This follows from proposition $5.1$ with repeated unions and minimums.
\end{proof}

\begin{thm}[5.3]
	If $\{h_n\}$ and $\{g_n\}$ are uniformly integrable and tight over $E$,
	then $\{\alpha h_n + \beta g_n\}$ is uniformly integrable and tight over $E$.
\end{thm}
\begin{proof}
	Let $\{h_n\}$ and $\{g_n\}$ be uniformly integrable and tight over $E$,
	and $\varepsilon>0$.

	Since $\{h_n\}$ is tight, there exists finite $E_0$ such that
	$\int_{E\setminus E_0} |h_n| < \sfrac\varepsilon \alpha$.
	Thus $\{\alpha h_n\}$ is tight.

	Since $\{h_n\}$ is uniformly integrable,
	there exists $\delta>0$
	such that for any $m(A)<\delta$ and $n$,
	$\int_A |h_n| < \sfrac\alpha\varepsilon$.
	Thus $\{\alpha h_n\}$ is uniformly integrable.

	It remains to show that $\{h_n+g_n\}$ is uniformly integrable and tight.
	Since each $\{h_n\}$ and $\{g_n\}$ are tight,
	there are finite sets $E_h$ and $E_g$ such that
	\begin{align*}
		\int_{E\setminus E_h} |h_n| &< \sfrac\varepsilon 2 \\
		\int_{E\setminus E_g} |g_n| &< \sfrac\varepsilon 2 \\
		\implies \int_{E\setminus(E_h\cup E_g)} |h_n+g_n|
		\leq \int_{E\setminus(E_h\cup E_g)} |h_n|+|g_n|
		&<\varepsilon
	\end{align*}
	Similarly, there are $\delta_h$ and $\delta_g$ such that
	for any $m(A_h)<\delta_h$ and $m(A_g)<\delta_g$,
	that $\int_{A_h} |h_n|<\sfrac\varepsilon 2$
	and $\int_{A_g} |g_n|<\sfrac\varepsilon 2$.

	So, for $\delta = \min\{\delta_h,\delta_g\}$
	and $m(A)<\delta$,
	$\int_A |h_n+g_n|<\int_A |h_n|+|g_n|<\varepsilon$.
\end{proof}

\begin{thm}[5.4]
	$f_n$ is uniformly integrable and tight over $E$
	if and only if for each $\varepsilon > 0$,
	there is a measurable subset $E_0$ of $E$ that has finite measure
	and a $\delta > 0$ such that for each measurable subset $A\subseteq E$
	and index $n$,
	\[ m(A \cap E_0) < \delta \implies \int_A |f_n|<\varepsilon\]
\end{thm}
\begin{proof}
	Let $f_n$ be a family of functions on $E$.

	($\implies$)
	Let $f_n$ be uniformly integrable and tight,
	and $\varepsilon>0$.
	Then there is a $\delta>0$ such that for each $f_n$
	and measurable $A\subseteq E$,
	if $m(A)<\delta$, then $\int_A|f_n|<\sfrac \varepsilon 2$.
	There is also a measurable subset $E_0$ such that
	$\int_{E\setminus E_0}|f|<\sfrac \varepsilon 2$.

	Now consider some $A'\subseteq E$ where $m(A'\cap E_0)<\delta$.
	By countable additivity of integration,
	\[ \int_{A'} |f_n|
		= \int_{A'\setminus E_0} |f_n| + \int_{A'\cap E_0} |f_n|
		< \int_{E\setminus E_0} |f_n| + \int_A |f_n|
		< \frac \varepsilon 2 + \frac\varepsilon 2
		=\varepsilon
	\]
	

	($\impliedby$)
	Suppose $\exists E_0\subseteq E$ with finite measure
	and a $\delta > 0$ such that for each measurable subset $A\subseteq E$
	and index $n$,
	$m(A \cap E_0) < \delta \implies \int_A |f_n|<\varepsilon$.

	First, take $A=E\setminus E_0$:
	Since $m((E\setminus E_0)\cap E_0) = m(\emptyset)=0$, $m(A\cap E_0)<\delta$ is true.
	So $\int_{E\setminus E_0} |f_n|<\varepsilon$,
	and $f_n$ is tight.

	Additionally, when $E_0=E$:
	\[ m(A)<\delta\implies \int_A |f_n|<\varepsilon\]
	Thus $f_n$ is uniformly integrable.
\end{proof}

\begin{thm}[5.5]
	$f_n$ is uniformly integrable and tight over $E$
	if and only if for each $\varepsilon > 0$,
	there are $\delta,r > 0$ such that for each open subset $\cal O\subseteq E$
	and index $n$,
	\[ m(\cal O \cap (-r,r)) < \delta \implies \int_{\cal O} |f_n|<\varepsilon\]
\end{thm}
\begin{proof}[Sketch]
	This hinges on Theorem $2.11$.
	$A$ is measurable, if and only if
	$\forall \varepsilon > 0, \exists\cal O\mbox{ open},m(\cal O\setminus A)<\varepsilon$.
\end{proof}

\begin{thm}[5.6]
	If $\{f_n\}\rightarrow f$ in measure on $E$ and $g$ is measurable on $E$ and finite\ae,
	then $\{f_n\}\rightarrow g$ in measure if and only if $f=g\ae$ on $E$.
\end{thm}
\begin{proof}
	Let $\{f_n\}\rightarrow f$ in measure on $E$
	and $g$ be measurable on $E$, finite\ae.

	($\implies$)
	Suppose $f\neq g$ on $E_0\subseteq E$ with $m(E_0)>0$.
	For every $x\in E_0$,
	there is an $\eta$ with $|f(x)-g(x)|>\eta$.
	\begin{align*}
		\lim_{n\rightarrow\infty}m\{x\in E\mid |f_n(x)-g(x)|>\eta\}
		&=
		\lim_{n\rightarrow\infty}m(E_0\cup\{x\in E\mid |f_n(x)-f(x)|>\eta\})
		> 0
	\end{align*}

	($\impliedby$)
	Suppose $g=f$ on $E\setminus E_0$, where $E_0$ has measure 0.
	For any index $n$:
	\begin{align*}
		\{ x\in E\mid |f_n(x)-g(x)|>\eta\}
		&\subseteq \{ x\in E\mid |f_n(x)-f(x)|>\eta\}\cup E_0 \\
		m( \{ x\in E\mid |f_n(x)-g(x)|>\eta\})
		&\leq m( \{ x\in E\mid |f_n(x)-f(x)|>\eta\})+0
	\end{align*}
	Thus $\lim_{n\rightarrow\infty} m\left(\left\{
		x\in E\mid |f_n(x)-g(x)|>\eta
	\right\}\right)=0$.

\end{proof}

\begin{thm}[5.7]
	Let $E$ have finite measure, $\{f_n\}\rightarrow f$ in measure on $E$,
	and $g$ be measurable on $E$, finite\ae~on $E$.
	Then
	\begin{itemize}[nosep]
	\item $\{f_n\cdot g\}\rightarrow f\cdot g$ in measure.
	\item $\{{f_n}^2\}\rightarrow f^2$ in measure.
	\item $\{g_n\}\rightarrow g$ in measure $\implies \{f_n\cdot g_n\}\rightarrow f\cdot g$ in measure.
	\end{itemize}
\end{thm}
\begin{proof}
	Let $E$ have finite measure, $\{f_n\}\rightarrow f$ in measure on $E$,
	and $g$ be measurable on $E$, finite\ae~on $E$.
	Let $\varepsilon>0$ be given.
	Now since $g$ is finite\ae, $g$ is bounded by some $M$
	on some $E\setminus E_0$, with $E_0<\sfrac\varepsilon 2$.

	So:
	\begin{align*}
		m(\{x\in E\mid |g(x)||\cdot f_n(x)-f(x)|>\eta\})
		&= m(\{x\in E\mid (M|\cdot f_n(x)-f(x)|)>\eta\}\cup E_0)
	\end{align*}
	Since $\{f_n\}\rightarrow f$ in measure,
	$m(\{x\in E\mid (M|\cdot f_n(x)-f(x)|)>\eta\})<\sfrac\varepsilon 2$.
\end{proof}

\begin{thm}[5.8]
	Fatou's Lemma, the Monotone Convergence Theorem, LDCT,
	and Vitali Convergence Theorem are valid if pointwise convergent\ae~is
	replaced by ``convergence in measure''.
\end{thm}
\begin{proof}
	(Fatou's Lemma):
	Let $\{f_n\},f$ be nonnegative and measurable,
	with $\{f_n\}\rightarrow f$ in measure on $E$.
	Let $h$ be a function with $0\leq h\leq f$.
	Take $h_n=\min\{h,f_n\}$.

	Let $\eta>0$ and $\varepsilon>0$ be given.
	There exists an index $M$ such that for all $n>M$,
	and $F=\{x\in E\mid |f_n(x)-f(x)|>\eta\}$,
	$m(F)<\varepsilon$.

	Take some $x\in E$.
	If $h(x)\leq f_n(x)$,
	then $h_n(x)=h(x)$ and $|h_n(x)-h(x)|<\eta$.
	Otherwise, $f_n(x)<h(x)\leq f(x)$.
	So if $|h_n(x)-h(x)|>\eta$ then $|f_n(x)-f(x)|>\eta$.
	Therefore $h$ is convergent in measure.
	By the bounded convergence theorem,
	$\int_E h\leq \lim\inf\int_E f_n$ as in the original proof.

	All of the other proofs rely on Fatou's Lemma.
\end{proof}

\begin{thm}[5.9]
	There exists $f$ finite and $\{f_n\}\rightarrow f$ pointwise\ae~such that
	$\{f_n\}\not\rightarrow f$ in measure.
\end{thm}
\begin{proof}
	Take $f_n=\chi_{(n,\infty)}$.
\end{proof}

\begin{thm}[5.10]
	Convergence in measure of sequences of functions on finite measure sets is maintained under linear combinations.
\end{thm}
\begin{proof}[Sketch]
	Take $\{\alpha f_n+\beta g_n\}$.
	Given $\eta,\varepsilon>0$,
	we can apply convergence in measure with $\sfrac \eta 2$ and $\sfrac \varepsilon {2\alpha}$ on $f_n$
	and $\sfrac \eta 2$ and $\sfrac \varepsilon {2\beta}$ on $g_n$, to find two indices $M_f$ and $M_g$.
	Take $n>\max\{M_f,M_g\}$.
\end{proof}

\end{document}
