\documentclass{article}
\usepackage[margin=1in]{geometry}
\input{include.d/macros}

\title{Analysis II}
\author{ Paul Modlin }
\date{\today}

\begin{document}
\begin{thm}[3.1]
	Let $f,g:[a,b]$ be continuous.
	If $f=g\ae$, then $f=g$ on $[a,b]$.
\end{thm}
\begin{proof}
	Let $f,g:[a,b]$ be continuous, and $x\in E\subseteq [a,b]$, where $m(E)=0$
	Suppose $f-g=0$ on $[a,b]\setminus E$.
	Let $\delta>0$ be given. Since $f$ and $g$ are continuous, $f-g$ is.
	Because $E$ is measure zero,
	for every neighborhood $(x-\sfrac 1 k,x+\sfrac 1 k)$,
	there exists an element $a_k \in (x-\sfrac 1 k,x+\sfrac 1 k)\cap[a,b]\setminus E$.
	Since $(f-g)(a_k)=0$ for every $k$ and $f-g$ is continuous, $(f-g)(x)=0$.

	Since $x\in E$ was arbitrary, $f=g$ on $[a,b]$.
\end{proof}

\begin{rem}[3.2]
	$f$ can be discontinuous on $D\cup E$, even though $f$ is continuous when restricted to $D$ or $E$.
	(Take the floor function on $[0,1)$, $[1,2)$.)
\end{rem}

\begin{rem}[3.3]
	If $E$ is measurable and $f:E$ is continuous on $E\setminus\{a_1,a_2,\ldots,a_n\}$,
	then $f$ is measurable: $f$ is measurable on all such $E\cap(a_k,a_{k+1})$,
	and $f$ is measurable on $\{a_1,a_2,\ldots,a_n\}$.
\end{rem}

\begin{rem}[3.4]
	Take $S\subseteq (0,\infty)$ to be a non-measurable set:
	\[f(x)=\begin{cases} x & x\in S\\-x&x\not\in S\end{cases}\]
\end{rem}

\begin{thm}[3.5]
	Let $E$ be measurable, and
	$f:E\rightarrow \bb R$ such that $\{x\in E\mid f(x)>q\}$ is measurable for every $q\in\bb Q$.
	Then $f$ is measurable.
\end{thm}
\begin{proof}
	Let $E,f$ as above. Choose $c\in\bb R$.
	Take a decreasing sequence $\{q_n\in\bb Q\}$ converging to $c$.
	\[\{x\in E\mid f(x)>c\}=\bigcup_{n=1}^\infty\{x\in E\mid f(x)>q_n\}\]
	Since unions of measurable sets are measurable, $f$ is measurable.
\end{proof}

\begin{thm}[3.6]
	Let $D$ be measurable, and $f:D\rightarrow \bb R$.

	$f$ is measurable if and only if $g:\bb R\rightarrow \bb R$ as defined below is measurable:
	\[g(x)=\begin{cases} f(x)&x\in D\\0&x\not\in D\end{cases}\]
\end{thm}
\begin{proof}[Sketch]
	First, $D$ is measurable if and only if $\bb R\setminus D$ is measurable.
	Second, use the union property.
\end{proof}

\begin{thm}[3.7]
	Let $D$ be measurable, and $f:D\rightarrow \bb R$.

	$f$ is measurable if and only if: $B$ Borel implies $f^{-1}(B)$ measurable.
\end{thm}
\begin{proof}
	If $\Omega = \{A\mid f^{-1}(A)\mbox{ measurable}\}$ is a $\sigma$-algebra,
	then by proposition 2,
	$f^{-1}(B)$ is measurable if and only if $f$ is measurable.

	Let $f$ be defined on a measurable domain $D$.
	First, $f^{-1}(f(D))=D$ is measurable, so $D\in\Omega$.
	Take $A,B\in\Omega$. Then $f^{-1}(A)$ and $f^{-1}(B)$ are measurable.
	\begin{align*}
		f^{-1}(A\cap B)&= f^{-1}(A)\cap f^{-1}(B) \\
		f^{-1}(A\cup B)&= f^{-1}(A)\cup f^{-1}(B) \\
		f^{-1}(f(D)\setminus A)&= D\setminus f^{-1}(A)
	\end{align*}
	Since measurable sets form a $\sigma$-algebra,
	all three of these are measurable.
	Thus $\Omega$ forms a $\sigma$-algebra.
\end{proof}

\begin{thm}[3.10]
	There exists $f$ measurable and $g$ continuous such that $f\circ g$ is non-measurable.
\end{thm}
\begin{proof}
	Let $\psi(x)=\phi(x)+x$, (the Cantor-Lesbesgue function plus the identity function).
	By proposition 21, $\psi$ maps a measurable set $V$ onto a nonmeasurable set $W$.
	Take $g(x)=\psi^{-1}(x)$. $g$ is continuous.
	Let $f(x)=\chi_V$. Now $f^{-1}((\sfrac 1 2,1+\sfrac 1 2))=V$, and $g^{-1}(V)=W$.

	Thus $f\circ g$ is non-measurable.
\end{proof}

\begin{thm}[3.11]
	Let $f$ be measurable, and $g:\bb R\rightarrow\bb R$ one-to-one, with $g^{-1}$ Lipschitz.

	Then $f\circ g$ is measurable.
\end{thm}
\begin{proof}
	Let $f$ be measurable, and $g:\bb R\rightarrow\bb R$ one-to-one, with $g^{-1}$ Lipschitz.

	Let $\cal O$ be open.
	$f^{-1}(\cal O)$ is measurable by proposition 2.
	$g^{-1}(f^{-1}(\cal O))$ is measurable by 2.38.
\end{proof}

\begin{thm}[3.12]
	Let $f$ be a bounded measurable function on $E$.

	Then for some pair of sequence of simple functions $\{\phi_n\}$ and $\{\psi_n\}$:
	\begin{itemize}[nosep]
	\item $\{\phi_n\}$ is increasing and converges uniformly to $f$ on $E$.
	\item $\{\psi_n\}$ is decreasing and converges uniformly to $f$ on $E$.
	\end{itemize}
\end{thm}
\begin{proof}
	Let $f$ be a bounded measurable function on $E$, with $|f|\leq M\in\bb N$.
	Let $n\in\bb N$
	By the Simple Approximation Lemma,
	there are simple functions $\phi_n$
	and $\psi_n$ such that
	\[
		\phi_n\leq f\leq \psi_n\mbox{ and }
		0\leq \psi_n-\phi_n<\frac 1 n
		\mbox{ on }E
	\]
	The sequences
	$\{\phi'_n = \sup\{\phi_n,\phi'_{n-1}\}\}$
	and $\{\psi'_n = \inf\{\psi_n,\psi'_{n-1}\}\}$
	converge uniformly to $f$.
\end{proof}

\begin{thm}[3.13]
	Let $f$ be a measurable function on $E$.
	There exists a sequences of semisimple functions $\{f_n\}$
	converging uniformly to $f$ on $E$.
\end{thm}
\begin{proof}[Sketch]
	\[f_n(x) = \frac {\lfloor n f(x) \rfloor} n \]
\end{proof}

\begin{thm}[3.14]
	Let $f$ be measurable on $E$, finite\ae, and $m(E)$ finite.

	For all $\varepsilon>0$, there is $F\subseteq E$ measurable
	such that $f|_F$ is bounded and $m(E\setminus F)<\varepsilon$.
\end{thm}
\begin{proof}
	Let $f$ be measurable on $E$, finite\ae, and $m(E)$ finite.
	Let $\varepsilon>0$ be given.
	Since $f$ is finite\ae, we can restrict $f$ to a $E'\subseteq E$,
	with $m(E')=m(E)$ and $f$ finite. By Theorem 2.11,
	there is a closed set $F\subseteq E'$ for which $m(E'\setminus F)<\sfrac \varepsilon 2$.
	Since $m(F)<\infty$, for some subset $F'$, $F'$ is bounded and $m(F\setminus F')<\sfrac\varepsilon 2$.
	Now $F'$ is compact, so $f|_{F'}$ is bounded, with $m(E\setminus F')<\varepsilon$.
\end{proof}

\begin{thm}[3.15]
	Let $f$ be measurable on $E$, finite\ae, and $m(E)<\infty$.

	For all $\varepsilon>0$, there is a measurable set $F\subseteq E$
	and a sequence of simple functions $\{\phi_n\}\rightarrow f$ uniformly
	with $m(E\setminus F)<\varepsilon$.
\end{thm}
\begin{proof}
	Let $f$ be measurable on $E$, finite\ae, and $m(E)<\infty$.

	By 3.14, there is a measurable subset $F$ where $f$ is bounded
	By 3.12, for all $\varepsilon>0$,
	there is and a sequence of simple functions $\{\phi_n\}$
	which converges uniformly to $f$.
\end{proof}

\begin{thm}[3.16]
	Let $I$ be a closed, bounded interval, and $E\subseteq I$ measurable.
	For all $\varepsilon>0$,
	there is a step function $h:I$ and $F\subseteq I$ measurable with
	\[ h=\chi_E\mbox{ on }F\mbox{ and }m(I\setminus F)<\varepsilon\]
\end{thm}
\begin{proof}
	Let $I$ be a closed, bounded interval, and $E\subseteq I$ measurable.
	Take $\varepsilon>0$. By Theorem 2.12,
	there is are disjoint unions of open intervals
	$\cal O =\bigcup\{I_1,I_2,\ldots,I_n\}\subseteq E$ and
	$\cal P=\bigcup\{J_1,J_2,\ldots,J_m\}\subseteq I\setminus E$ for which
	$m(E\setminus\cal O)+<\sfrac \varepsilon 2$
	and $m((I\setminus E)\setminus\cal P)<\sfrac\varepsilon 2$.
	Define $h$ on $\cal O\cup\cal P$:
	\[ h(x) =
		\begin{cases}
			1 & x\in\cal O \\
			0 & x\in\cal P
		\end{cases}
	\]
	Finally, $h$ is a step function with $h=\chi_E$ on $\cal O\cup\cal P$
	and $m(I\setminus(\cal O\cup\cal P))<\varepsilon$.
\end{proof}

\begin{thm}[3.20]
	For any $A,B$:
	\begin{align}
		\label{3.20.1}
		\chi_{A\cap B}&=\chi_A\cdot\chi_B \\
		\label{3.20.2}
		\chi_{A\cup B}&=\chi_A+\chi_B-\chi_A\cdot\chi_B \\
		\label{3.20.3}
		\chi_{X\setminus A}&=1-\chi_A
	\end{align}
\end{thm}
\begin{proof}
	If $x\in A$, $x\not\in B$,
	then $\chi_A\cdot\chi_B=1\cdot 0=0$.
	This shows (\ref{3.20.1}).

	Since $x\in A \iff x\in X\setminus A$,
	(\ref{3.20.3}) holds.

	Applying De Morgan's Laws, (\ref{3.20.2}) holds.
\end{proof}

\begin{thm}[3.21]
	If $\{f_n:E\rightarrow\bb R\}$ is a sequence of measurable functions,
	then the folowing are all measurable:
	\[ \inf\{f_n\},\sup\{f_n\},\lim\inf\{f_n\},\lim\sup\{f_n\}\]
\end{thm}
\begin{proof}
	Since $-f_n$ is measurable iff $f_n$ is measurable,
	$\inf\{f_n\}$ is measurable iff $\sup\{f_n\}$ is measurable,
	and $\lim\inf\{f_n\}$ is measurable iff $\lim\sup\{f_n\}$ is measurable.

	Let $c\in\bb R$ be given. If $\inf\{f_n\}(x)<c$,
	then for some index $k$, $f_k(x)<c$.

	Thus $\{x\mid\inf\{f_n\}(x)<c\} = \bigcup_{k=1}^\infty\{x\mid f_k(x)<c\}$.
	Since $\{x\mid f_k(x)<c\}$ are measurable,
	$\{x\mid\inf\{f_n\}(x)<c\}$ is measurable.
\end{proof}

\begin{thm}[3.25]
	Let $F$ be closed, and
	$f:F\rightarrow\bb R$ be continuous.
	Then $f$ has a continuous extension to $\bb R$
\end{thm}
\begin{proof}
	Let $F$ be closed, and $f:F\rightarrow\bb R$ be continuous.
	Now $\bb R\setminus F$ is open,
	and so is a countable disjoint collection of open intervals $\{(a_n,b_n)\}$.
	Notably, $a_n,b_n\in F$.
	Define $f'$ piecewise:
	\[ f'(x)=\begin{cases}
		f(x) & x \in F \\
		\frac {f(b_k)-f(a_k)}{b_k-a_k}(x-b_k)+f(b_k)
		& x \in (a_k,b_k)
	\end{cases}\]
	Now $f'$ agrees with $f$ on $F$, is linear on $\bb R\setminus F$,
	$\lim {x\rightarrow a_k} f'(x)=f'(a_k)$, and
	$\lim {x\rightarrow b_k} f'(x)=f'(b_k)$.

	Thus, $f'$ is continuous.
\end{proof}

\begin{thm}[3.26]
	Take $f$, $E$ and $F$ as in Lusin's Theorem.
	\begin{enumerate}[nosep,label= (\alph*)]
	\item $f|_F$ is continuous.
	\item $f|_E$ may be discontinuous on all $E$.
	\end{enumerate}
\end{thm}
\begin{proof}
	Since $f|_F=g$ and $g$ is continuous, $f|_F$ is continuous.

	Take $f$ to be the Dirichlet function on the Cantor set.
	Take $g$ on the empty set.
\end{proof}

\begin{thm}[3.27]
	There exists $\{f_n\}\rightarrow f$ on a measurable set $E$
	and $\varepsilon > 0$ where for any $F\subseteq E$ closed
	with $m(E\setminus F)<\varepsilon$,
	$\{f_n\}$ does not converge uniformly to $f$ on $F$.
\end{thm}
\begin{proof}
	Take $f=0$ on $\bb R$, and $\{f_n=\frac x n\}$.
	For any closed $F$ where $m(\bb R\setminus F)<\varepsilon$,
	Therefore $F$ is unbounded. Take $\delta=1$, and any $n$.

	Since $F$ is unbounded, we can find $x\in F$
	such that $|x|>n$.
	$|f_n(x)-f(x)|>\delta$.
	Thus $\{f_n\}$ does not converge uniformly to $f$.
\end{proof}

\begin{thm}[3.28]
	If $f_n$ converges pointwise{\ae} to $f$, then Egoroff's Theorem holds.
\end{thm}
\begin{proof}
	Take $E'$ to be where $f_n$ converges pointwise.
	Thus Egoroff's Theorem holds for $E'$,
	and we find $F\subseteq E'$ closed
	with $\{f_n\}\rightarrow f$ uniformly
	and $m(E'\setminus F)<\varepsilon$.

	Since $m(E\setminus E')=0$, $m(E\setminus F)<\varepsilon$.
\end{proof}

\begin{thm}[3.29]
	If $E$ has infinite measure, then Lusin's Theorem holds.
\end{thm}
\begin{proof}
	Let $\varepsilon>0$.
	For each $\{E_n=E\cap[n,n+1-\varepsilon2^{-|n|-3})\}$,
	apply Lusin's Theorem with $\varepsilon_n=\varepsilon 2^{-|n|-3}$
	to get disjoint $\{F_n\subseteq E_n\}$ and $\{g_n\}$ continuous.

	Between successive $E_n$, there is an open interval of length $2^{-|n|-3}$,
	so $g(x)=\{ g_k(x), x\in F_k$ is continuous.
	\begin{align*}
		m(\bb R\setminus \bigcup_{n\in\bb Z}E_n)
		&=\frac {3\varepsilon}8 \\
		m(\bigcup_{n\in\bb Z}E_n\setminus\bigcup_{n\in\bb Z} F_n)
		&<\frac {3\varepsilon}8 \\
		\mbox{so: }
		m(\bb R\setminus\bigcup_{n\in\bb Z} F_n)&<\frac {3\varepsilon}4<\varepsilon\fs
	\end{align*}

\end{proof}
\end{document}
