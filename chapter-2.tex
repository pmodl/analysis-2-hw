\documentclass{article}
\usepackage[margin=1in]{geometry}
\input{include.d/macros}

\title{Analysis II}
\author{ Paul Modlin }
\date{\today}

\begin{document}

\begin{thm}[2.1]
	If $A\subseteq B$, then $m(A)\leq m(B)$.
\end{thm}

\begin{proof}
	Let $A\subseteq B$ be sets such that $m(A)$ and $m(B)$ exist.
	Suppose $A = B$. Then $m(A) = m(B)$.

	Now suppose $A\subset B$. For some set $E$, $B = A \cup E$.
	Since measure is countably additive over unions,
	$m(B) = m(A) + m(E) \geq m(A)$.
\end{proof}

\begin{thm}[2.2]
	Let $\cal A$ be a collection, and $A\in\cal A$ a set.
	If $m(A)\leq \infty$, then $m(\emptyset) = 0$.
\end{thm}

\begin{proof}
	Let $\cal A$ be a collection,
	and $A\in\cal A$ a set with $m(A)=M\in\bb R$
	Since $A \cup \emptyset = A$,
	\[ M = m(A) = m(A\cup \emptyset) = m(A) + m(\emptyset) \]
	Therefore, $m(\emptyset) = 0$.
\end{proof}

\begin{thm}[2.3]
	If $\{E_k\}^\infty_{k=1}\subseteq \cal A$, then
	$m(\bigcup^\infty_{k=1} E_k)\leq\sum^\infty_{k=1}m(E_k)$.
\end{thm}

	\begin{proof}
	Let $\{E_k\}^\infty_{k=1}\subseteq \cal A$.
	For each $k\in\bb N$, define $E'_k$:
	\[ E'_k = E_k\setminus \left(
			\bigcup\limits_{i=1}^{k-1} E_i
	\right) \]
	The following are true:
	\begin{enumerate}[noitemsep]
	\item Clearly, $E'_k \subseteq E_k$, so $m(E'_k) \leq m(E_k)$.
	\item Also, $E'_k$ is disjoint with all sets $\{E_1,\ldots, E_{k-1}\}$.
		By induction this shows that all $E'_k$ are disjoint.
	\item $E'_1 = E_1$.
	\item By definition of $E_n$, $E'_n \cup (\bigcup_{k=1}^{n-1}E_k) = \bigcup_{k=1}^n E_n$.
	\end{enumerate}
	By induction with (3) and (4), for any $n\in\bb N$:
	\[ \bigcup\limits_{k=1}^n E'_k = \bigcup\limits_{k=1}^n E_k \]
	By (2), we can apply our additive property:
	\[ m\left(\bigcup\limits_{k=1}^n E_k\right)
		= m\left(\bigcup\limits_{k=1}^n E'_k\right)
		= \sum\limits_{k=1}^n m(E'_k)
		\leq \sum\limits_{k=1}^n m(E_k)
	\]
	Taking the limit as $n$ approaches $\infty$, we have 
	$m(\bigcup^\infty_{k=1} E_k)\leq\sum^\infty_{k=1}m(E_k)$.
	\end{proof}

\begin{thm}[2.6]
	Prove $m^*([0,1]\setminus\bb Q)=1$.
\end{thm}
\begin{proof}
	\begin{enumerate}[noitemsep]
	\item By proposition 1, $m*([0,1])=1$.
	\item By monotonicity, $m^*([0,1]\setminus\bb Q) \leq m*([0,1])$.
	\item By subadditivity, $m*([0,1])\leq m^*([0,1]\setminus\bb Q)+m^*([0,1]\cap\bb Q)$.
	\item Since $[0,1]\cap \bb Q$ is countable, $m^*([0,1]\cap\bb Q)=0$.
	\end{enumerate}
	Therefore, we have the following:
	\begin{align*}
		1
		&=m^*([0,1]) & \mbox{ by (1)} \\
		&\leq m^*([0,1]\setminus\bb Q)+m^*([0,1]\cap\bb Q) & \mbox{ by (3)} \\
		&= m^*([0,1]\setminus\bb Q)+0 & \mbox{ by (4)} \\
		&\leq m^*([0,1]) = 1 & \mbox{ by (2,1)}
	\end{align*}
	Since $1\leq m^*([0,1]\setminus\bb Q)\leq 1$, $m^*([0,1]\setminus\bb Q) = 1$.
\end{proof}

\begin{thm}[2.7]
	A set $G\subseteq \bb R$ is said to be $G_\delta$
	if $G$ is an intersection of a countable collection of open sets.
	Show that if $E$ is bounded, then:
	\[ (\exists G)\left( G\mbox{ is }G_\delta \land E\subseteq G \land m^*(G)=m^*(E) \right) \]
\end{thm}
\begin{proof}
	Let $E$ be bounded and $\varepsilon > 0$ be given.
	By definition of Lebesgue outer measure:
	\[
		m^*(E) = \inf\left\{\sum_{k=1}^\infty l(I_k)
		\mid E\subseteq \bigcup_{k=1}^\infty I_k\right\}
	\]
	By definition of infimum, there is a sequence of collections
	$\{{I_j}_k \mid (\forall j)(E \subseteq \bigcup_{k=1}^\infty {I_j}_k)\}$,
	and an index $M$ such that for all $n>M$,
	\[ \sum_{k=1}^\infty l(I_k) < m^*(E) + \varepsilon \]
	Let $G = \bigcap_{j=1}^\infty\left(\bigcup_{k=1}^\infty {I_j}_k \right)$.
	Note that $G$ is $G_\delta$, and $E\subseteq G$.
	By definition of intersection, $G\subseteq \bigcup_{k=1}^\infty {I_{M+1}}_k$.
	
	By monotonicity:
	\[
		m^*(E) \leq m^*(G) \leq m^*(\bigcup_{k=1}^\infty {I_{M+1}}_k)
		\leq\sum_{k=1}^\infty l({I_{M+1}}_k)< m^*(E)+\varepsilon
	\]
	Since $\varepsilon$ was arbitrary, $m^*(E) = m^*(G)$.
\end{proof}

\begin{thm}[2.8]
	Let $\{I_1,I_2,I_3,\ldots,I_n\}$ be open intervals covering $[0,1]\cap \bb Q$.
	Then $\sum_{k=1}^n m^*(I_k)\geq 1$.
\end{thm}
\begin{proof}
	Let $\{I_1,I_2,I_3,\ldots,I_n\}$ be open intervals covering $[0,1]\cap \bb Q$,
	and suppose $\sum_{k=1}^n m^*(I_k)<1$.
	Without loss of generality, order the intervals such that
	$\inf I_k \leq \inf I_{k+1}$ for all $k$.

	Now if for some $k$, $\sup I_k \geq \sup I_{k+1}$, then $I_{k+1}\subseteq I_k$.
	Discard all such redundant $I_{k+1}$ and re-index from $1$ to $m$
	Since $0\in\bigcup\{I_k\}$, $\inf I_1\leq 0$.
	Since $1\in\bigcup\{I_k\}$, $\sup I_m\geq 1$.
	For brevity, we will consider $\{\inf I_k\} = \{a_k\}$, and $\{\sup I_k\}=\{b_k\}$.

	\begin{align*}
		\sum_{k=1}^m m^*(I_k) = \sum_{k=1}^m m^*(I_k)
		&= (b_1 - a_1) + (b_2 - a_2) + \cdots + (b_m - a_m) \\
		&\leq b_1-0 + b_2-a_2+\cdots+1-a_m \\
		&= 1 + (b_1-a_2)+(b_2-a_3)+\cdots+(b_{m-1}-a_m)
	\end{align*}
	Now $\sum_{k=1}^m m^*(I_k)<1$,
	so for at least one $i$, $b_i < a_{i+1}$.
	By density of $\bb Q$, $\exists q\in(b_i,a_{i+1})\cap \bb Q$.

	Since $a_{i+1}\leq a_{i+2}\leq\cdots\leq a_m$,
	$q\not\in\bigcup_{k=i+1}^m I_k$.

	Since $b_i\geq b_{i-1}\geq\cdots\geq b_1$,
	$q\not\in\bigcup_{k=1}^i I_k$.

	Thus, $q\not\in\bigcup_{k=1}^m$, and $\{I_k\}$ does not cover $[0,1]\cap \bb Q$.
\end{proof}

\begin{thm}[2.9] $m^*(A)=0\implies m^*(A\cup B)=m^*(B)$. \end{thm}
\begin{proof}
	Let $m^*(A)=0$.
	\begin{align*}
		m^*(B)&\leq m^*(A\cup B) &\mbox{ by monotonicity of $m^*$} \\
		&\leq m^*(A)+m^*(B) &\mbox{ by subadditivity of $m^*$} \\
		&= 0+m^*(B)
	\end{align*}
	Since $m^*(B)\leq m^*(A\cup B)\leq m^*(B)$, $m^*(A\cup B)=m^*(B)$.
\end{proof}

\begin{thm}[2.11]\label{2.11}
	If a $\sigma$-algebra of subsets of $\bb R$ contains all intervals of the form $(a,\infty)$,
	then it contains all intervals.
\end{thm}
\begin{proof}
	Let $S\subseteq \cal P(\bb R)$ be a $\sigma$-algebra,
	where $\{(a,\infty)\mid a\in\bb R\}\subseteq S$.
	Let $a<b$ be arbitrary.

	Since $S$ is closed under complements,
	$\bb R \setminus (b,\infty) = (-\infty,b]\in S$.

	Since $S$ is closed under countable unions,
	$\bigcup_{k=1}^\infty \{(a-\sfrac 1 k,\infty)\}=[a,\infty)\in S$.

	Thus, all the following are elements of $S$:
	\begin{align*}
		(-\infty, b]\cap [b,\infty)&=\{b\} \\
		(-\infty, a]\cap [b,\infty)&=\{a\} \\
		(-\infty, b]\setminus \{b\} &= (-\infty,b) \\
		(-\infty, b]\cap [a,\infty)&=[a,b] \\
		(-\infty, b]\cap (a,\infty)&=(a,b] \\
		(a,b]\setminus\{b\} &= (a,b) \\
		[a,b]\setminus\{b\} &= [a,b)
	\end{align*}
\end{proof}

\begin{thm}[2.12]
	Every interval is a Borel set.
\end{thm}
\begin{proof}
	Since $(a,\infty)$ is open, $(a,\infty)$ is in the Borel $\sigma$-algebra.
	By~\ref{2.11}, every interval is in the Borel $\sigma$-algebra.
\end{proof}

\begin{thm}[2.14]\label{2.14}
	If $E$ has positive outer measure,
	then $\exists A\subseteq E$ bounded with positive outer measure.
\end{thm}
\begin{proof}
	Let $E$ have positive outer measure.
	Since $\bigcup \{[k,k+1)\mid k\in\bb Z\} = \bb R$,
	$E = \bigcup_{k\in\bb Z} (E\cap [k,k+1)$.

	By subadditivity:
	\[
		0<m^*(E) = m^*\left(\bigcup_{k\in\bb Z} E\cap [k,k+1)\right)
		\leq \sum_{k\in\bb Z} m^*\big(E\cap[k,k+1)\big)
	\]
\begin{rem}
	Since $0<m^*(E)\leq\sum_{k\in\bb Z} m^*\big(E\cap[k,k+1)\big)$,
	at least one such $E\cap[k,k+1)\subseteq E$ has positive measure.
\end{rem}
\end{proof}

\begin{thm}[2.15]
	If $E$ has finite measure and $\varepsilon > 0$,
	then $E$ is the disjoint union of a finite number of measurable sets,
	each with measure at most $\varepsilon$.
\end{thm}
\begin{proof}
	Let $E$ have finite measure, and $\varepsilon > 0$.
	If $m^*(E)\leq\varepsilon$, then $E=\bigcup\{E\}$
	is a disjoint union of measureable sets with measure at most $\varepsilon$.

	So suppose $m^*(E)>\varepsilon$.
	By~\ref{2.14},
	\[ m^*(E)\leq\sum_{k\in\bb Z} m^*\big(E\cap[k,k+1)\big)\fs\]
	Equivalently, for some ordering $\{a_n\}$ of $\bb Z$,
	\[ m^*(E)\leq\sum_{n=1}^\infty m^*\big(E\cap[a_n,a_n+1)\big) \]
	So for some index $M\in \bb N$,
	\[\sum_{n=1}^M m^*\big(E\cap[a_n,a_n+1)\big) +\varepsilon > m^*(E)\]
	Since all such $[a_n,a_n+1)$ are disjoint, by Proposition 6:
	\begin{equation}\label{215p6}
		\sum_{n=1}^M m^*\big(E\cap[a_n,a_n+1)\big) +\varepsilon
		= m^*\left( E \cap
			\left[\bigcup_{n=1}^M [a_n,a_n+1) \right]
		\right) + \varepsilon
		> m^*(E)
	\end{equation}
	Let $\bigcup_{n=1}^M [a_n,a_n+1)=A$.
	Note that $A$ is bounded, and in particular $A \subseteq [x,y)$.
	By definition of measurability:
	$m^*(E) = m^*(E\cap A) + m^*(E\setminus A)$.
	\begin{align*}
		m^*(E\cap A) + m^*(E\setminus A)
		= m^*(E)
		&< m^*(E\cap A) + \varepsilon & \mbox{ by Eq~\ref{215p6}}\\
		\implies m^*(E\setminus A) &< \varepsilon
	\end{align*}
	Finally, take the finite disjoint union
	\[ A =\bigcup_{i=0}^{\frac {y-x}\varepsilon}\left\{ A \cap
			\Big[x+\varepsilon i, x+\varepsilon(i+1)\Big)
	\right\} \]
	\[ E = \bigcup_{i=0}^{\frac {y-x}\varepsilon}\left\{ A \cap
			\Big[x+\varepsilon i, x+\varepsilon(i+1)\Big)
	\right\} \cup E\setminus A \]
\end{proof}

\begin{thm}[2.16 (Theorem 11)]
	$E$ is measurable:
	\begin{itemize}[nosep]
		\item[(iii) $\iff$]for every $\varepsilon > 0$,
			$\exists F\subseteq E, m^*(E\setminus F)<\varepsilon$
		\item[(iv) $\iff$]there is an $F_\delta$ set $F\subseteq E$
			such that $m^*(E\setminus F) = 0$.
	\end{itemize}
\end{thm}
\begin{proof}[Proof]
	By the following equivalences,
	we find that (iii) and (i) are equivalent:
	\begin{align*}
		E \mbox{ is measurable} &\iff \bb R\setminus E \mbox{ is measurable} \\
		E\setminus F &= (\bb R\setminus F) \setminus (\bb E \setminus G) \\
		F \subseteq E &\iff (\bb R\setminus E) \subseteq (\bb R \setminus F) \\
		F \isc &\iff (\bb R \setminus F)\iso
	\end{align*}
	These imply that $m^*(E\setminus F)=m^*((\bb R \setminus F)\setminus(\bb R \setminus E))$.

	Next, we must show that $F$ is $F_\sigma$ if and only if $\bb R\setminus F$ is $G_\delta$.
	\begin{align*}
		F\mbox{ is $F_\sigma$}
		&\iff (\exists \{A_k\mbox{ closed}\} )(F =\bigcup_{k=1}^\infty A_k) \\
		&\iff (\exists \{A_k\mbox{ closed}\} )(F =\bb R \setminus \bigcap_{k=1}^\infty (\bb R\setminus A_k)) \\
		&\iff \bb R \setminus F\mbox{ is $G_\delta$}
	\end{align*}
\end{proof}

\begin{thm}[2.17]
	Let $\varepsilon > 0$.
	$E$ is measurable if and only if there is a closed set $F$, and an open set $\cal O$ such that
	$F \subseteq E\subseteq\cal O$ and $m^*(\cal O\setminus F)<\varepsilon$.
\end{thm}
\begin{proof}
	($\implies$)
	Let $\varepsilon > 0$, and $E$ measurable.
	By 2.16(i,iii) there exists a closed set $F\subseteq E$ and an open set $\cal O,E\subseteq \cal O$ such that
	$m^*(E\setminus F) <\sfrac\varepsilon 2$
	and $m^*(\cal O\setminus E) <\sfrac\varepsilon 2$.
	Since $\cal O$ and $F$ are measurable, 
	\[ m^*(\cal O\setminus F)
		= m^*((\cal O\setminus F)\cap E) + m^*((\cal O \setminus F)\setminus E)
		= m^*(E \setminus F) + m^*(\cal O \setminus E)
		< \sfrac \varepsilon 2 + \sfrac\varepsilon 2 = \varepsilon
	\]
\end{proof}
\begin{proof}
	($\impliedby$)
	Let $\varepsilon>0$, $E$ be a set, $F$ be closed, and $\cal O$ be open.
	Suppose $F\subseteq E\subseteq\cal O$, and $m^*(\cal O\setminus F)<\varepsilon$.
	Since $F\subseteq E$, $m^*(E\setminus F)\leq m^*(\cal O\setminus F)<\varepsilon$.
	Therefore $E$ is measurable.
\end{proof}

\begin{thm}[2.18]
	Let $E$ have finite outer measure.
	$E$ is measurable if and only if
	there is an $F_\sigma$ set $F$ and a $G_\delta$ set $G$ such that
	$F\subseteq E\subseteq G$ and $m^*(F)=m^*(E)=m^*(G)$.
\end{thm}
\begin{proof}
	($\impliedby$) Suppose
	there is an $F_\sigma$ set $F$ and a $G_\delta$ set $G$ such that
	$F\subseteq E\subseteq G$ and $m^*(F)=m^*(E)=m^*(G)$.

	By the excision property with $F$ and $G$, $m^*(G\setminus F)=m*(G)-m^*(F)=0$.
	Since outer measure is monotone, $m^*(G\setminus E)\leq m^*(G\setminus F)=0$.
	Thus $E$ is measurable by 11(ii).

	Now suppose $E$ is measurable.
	Then by 11(ii) and 11(iv), $F$ and $G$ exist which satisfy the conditions desired.
\end{proof}

\begin{thm}[2.19]
	Let $E$ be a non-measurable set with finite outer measure.
	There exists $\cal O$ open with $m^*(O\setminus E) > m^*(\cal O)-m^*(E)$.
\end{thm}
\begin{proof}
	Suppose $E$ is non-measurable with finite outer measure.
	Then for some set $A,E\subseteq A$, and some $\varepsilon > 0$:
	\begin{align*}
		m^*(A)&< m^*(A\cap E)+m^*(A\setminus E) \\
		m^*(A)&< m^*(E)+m^*(A\setminus E) \\
		m^*(A)+\varepsilon&=m^*(E)+m^*(A\setminus E)
	\end{align*}
	By definition of outer measure, there is an open set $\cal O,A\subseteq\cal O$
	with $m^*(\cal O)<m^*(A)+\sfrac\varepsilon 2$.
	So:
	\begin{align*}
		m^*(\cal O)+\sfrac\varepsilon 2 &= m^*(E)+m^*(A\setminus E) \\
		&\leq m^*(E)+m^*(\cal O\setminus E) \\
		m^*(\cal O\setminus E)&>m^*(\cal O)-m^*(E)
	\end{align*}
\end{proof}

\begin{thm}[2.20 (Lebesgue)]
	Let $E$ have finite outer measure.
	$E$ is measurable if and only if
	for every open, bounded inteval $(a,b)$,
	\[ b-a = m^*((a,b)\cap E)+m^*((a,b)\setminus E) \]
\end{thm}
\begin{proof}
	Firstly, if $E$ is measurable,
	the property $b-a = m^*((a,b)\cap E)+m^*((a,b)\setminus E)$ holds
	because $(a,b)$ is open.

	Now suppose $b-a = m^*((a,b)\cap E)+m^*((a,b)\setminus E)$ holds
	for every $(a,b)\subseteq \bb R$.
	Let $\varepsilon > 0$ be given,
	and choose $\cal O$ open with $E\subseteq \cal O$
	and $m^*(\cal O\setminus E)<\varepsilon$.
	Then for some finite disjoint collection of intervals $\{(a_i,b_i)\}$,
	$\cal O = \bigcup_{i=1}^{n} (a_i,b_i)$.
	\begin{align*}
		m^*(\cal O) = \sum_{i=1}^n l((a_i,b_i))
		&= \sum_{i=1}^n \left( m^*((a_i,b_i)\cap E)+m^*((a_i,b_i)\setminus E)\right) \\
		&= \sum_{i=1}^n m^*((a_i,b_i)\cap E)+\sum_{i=1}^n m^*((a_i,b_i)\setminus E) \\
		&= m^*(\bigcup_{i=1}^n (a_i,b_i)\cap E)+ m^*(\bigcup_{i=1}^n(a_i,b_i)\setminus E) &\mbox{ by Prop 6} \\
		&= m^*(\cal O\cap E)+m^*(\cal O \setminus E)
	\end{align*}
	Thus, by theorem 11(i), $E$ is measurable.
\end{proof}

\begin{thm}[2.24]
	If $E_1$ and $E_2$ are measurable, then
	\[ m(E_1\cup E_2)+m(E_1\cap E_2)=m(E_1)+m(E_2) \]
\end{thm}
\begin{proof}
	\begin{align*}
		E_1\cup E_2
		=\,& (E_1\setminus E_2)\cup (E_2\setminus E_1) \cup (E_1 \cap E_2)
		& \mbox{ disjoint union} \\
		& \\
		& m(E_1\cup E_2)+m(E_1\cap E_2) \\
		=\,& [m(E_1\setminus E_2)+m(E_2\setminus E_1)+m(E_1 \cap E_2)]+m(E_1\cap E_2)
		& \mbox{ finite additivity} \\
		=\,& m(E_1)-m(E_1\cap E_2) + m(E_2)-m(E_1\cap E_2)+ 2 m(E_1\cap E_2)
		& \mbox{ definition measurable}\\
		=\,& m(E_1)+m(E_2)
	\end{align*}
\end{proof}

\begin{thm}[2.25]
	There exists a decreasing sequence $\{B_k\}$
	such that $m(\bigcap_{k=1}^\infty B_k) \neq m(\lim_{k\rightarrow\infty}B_k)$
\end{thm}
\begin{proof}
	Let $\{B_k\} = \{ (n-\sfrac 1 {2k},n+\sfrac 1{2k}), n\in\bb Z\}$.
	Note
	$m\left(\bigcap_{k=1}^\infty\right)=m(\bb Z)=0$, but for every $k\in\bb N$, $m(B_k)=\infty$.
\end{proof}

\begin{thm}[2.26]
	Let $\{E_k\}_{k=1}^\infty$ be a countable disjoint collection of measurable sets.
	\[ m^*\left(A\cap \bigcup_{k=1}^\infty E_k\right)=\sum_{k=1}^\infty m^*(A\cap E_k) \]
\end{thm}
\begin{proof}
	By countable subaddativity, 
	$m^*\left(A\cap \bigcup_{k=1}^\infty E_k\right)\leq\sum_{k=1}^\infty m^*(A\cap E_k)$.

	\begin{align*}
		m^*\left(A\cap \bigcup_{k=1}^\infty E_k\right)
		&\geq m^*\left(A\cap \bigcup_{k=1}^n E_k\right) & \mbox{ for all n} \\
		&=\sum_{k=1}^n m^*(A\cap E_k) & \mbox{ by prop 6:}
	\end{align*}
	Since this holds for every $n$, in the limit we have:
	$m^*\left(A\cap \bigcup_{k=1}^\infty E_k\right)\geq\sum_{k=1}^\infty m^*(A\cap E_k)$
	as desired.
\end{proof}

Let $M'\subseteq \cal P(\bb R)$ be a $\sigma$-algebra.
Let $m':M'\rightarrow [0,\infty)$ be a countably additive function,
with $m'(\emptyset)=0$.
\begin{thm}[2.27(i)]
	$m'$ is finitely additive, monotone, countably monotone,
	and possesses the excision property.
\end{thm}
\begin{proof}[Sketch]
	Let $M'$ and $m'$ be as above.
	Clearly $m'$ is finitely additive since $m'$ is countably additive.

	If $A,B\in M'$, then $A\cap B, A\setminus B, B\setminus A, A\cup B\in M'$ as well.
	If $A\subseteq B$, then by additivity, $m'(A) + m'(B\setminus A) = m'(B)$.
	Thus the excision property holds, and $m'$ is monotone.
\end{proof}
\begin{thm}[2.27(ii)]
	Let $\{A_k\}$ be ascending and $\{B_k\}$ be descending.
	$m'\left(\bigcup_{k=1}^\infty A_k\right)=\lim_{k\rightarrow\infty}m'(A_k)$, and
	$m'\left(\bigcap_{k=1}^\infty B_k\right)=\lim_{k\rightarrow\infty}m'(B_k)$.
\end{thm}
\begin{proof}[Sketch]
	The proof tor this is exactly as the proof for theorem 15 on p.45.
\end{proof}

\begin{thm}[2.28]
	Countable additivity of measure follows from finite additivity and continuity.
\end{thm}
\begin{proof}[Sketch]
	Given an countable, disjoint collection of measurable sets $\{U_k\}$,
	construct an increasing union $\{A_k = \bigcup_{i=1}^k U_i\}$.
	By continuity and finite additivity:
	\[m\left(\bigcup_{k=1}^\infty U_k\right)= \lim_{k\rightarrow\infty} m(A_k) = \sum_{k=1}^\infty m(U_k)\]
\end{proof}

\begin{thm}[2.29] The following are true:
	\begin{enumerate}[label=(\alph*),nosep]
		\item Rational equivalence is an equivalence relation.
		\item $\{0\}$ is a choice set on $\bb Q$
		\item $a\sim b\iff a-b\not\in\bb Q$ is not an equivalence relation on $\bb R$.
		\item $a\sim b\iff a-b\not\in\bb Q$ is an equivalence relation on $\bb Q$.
	\end{enumerate}
\end{thm}
\begin{proof}[Sketch]:
	\begin{enumerate}[label=(\alph*),nosep]
		\item This follows from addition closed on $\bb Q$.
		\item $0\sim q \implies q\in\bb Q$.
		\item Transitivity does not hold:
			$\pi-1\not\in\bb Q$, and $1-(\pi+1)\not\in\bb Q$,
			but $\pi-(\pi+1)\in\bb Q$.
		\item This is the identity relation.
	\end{enumerate}
\end{proof}

\begin{thm}[2.30]
	If $m(A)>0$, then any choice set $\cal C_A$ on $A$ is uncountably infinite.
\end{thm}
\begin{proof}
	Suppose $\cal C_A$ was countable.
	For each $c\in\cal C_A$, $\bb Q+c \cap A$ is countable,
	therefore $\bigcup_{c\in\cal C_A} (\bb Q+c\cap A)$ is countable.
	Since $\cal C_A$ is a choice set on $A$,
	$\bigcup_{c\in\cal C_A}(\bb Q+c\cap A)=A$.
	Thus $A$ is countable, and $m(A)=0$.
\end{proof}

\begin{rem}[2.31]
	It suffices to consider only the bounded case in Vitali's Theorem,
	because every set $E$ with positive measure has a bounded subset $E'$ with positive measure.
	Finding a non-measurable subset $U\subseteq E'$ implies $U\subseteq E$.
\end{rem}
\begin{rem}[2.32]
	Lemma 16 does not remain true if $\Lambda$ is unbounded or finite:
	$E=[0,1),\Lambda=\bb Z$, or $E=[0,1)$, $\Lambda=\{0\}$.
	Lemma 16 does remain true if $\Lambda$ is uncountably infinite.
\end{rem}

\begin{thm}[2.33]
	Let $E$ be a nonmeasurable, with finite outer measure.
	For some $G_\delta$ set $G$:
	\begin{enumerate}[label=(\alph*),nosep]
	\item $E\subseteq G$
	\item $m^*(E) = m^*(G)$
	\item $m^*(G\setminus E)>0$
	\end{enumerate}
\end{thm}
\begin{proof}[Sketch]
	Let $E$ be a nonmeasurable, with finite outer measure.
	By the definition of outer measure,
	there exists a sequence of open sets $\{A_i = \bigcup_{k=1}^\infty I_k\}$
	where $m^*(A_i)\leq m^*(E)\leq m^*(A_i)+\sfrac 1 i$
	and $E\subseteq A_i$.

	Take $G=\bigcap_{i=1}^\infty A_i$.
	$E\subseteq G$ and $m^*(E)=m^*(G)$.

	If $m^*(G\setminus E)=0$,
	then $E$ is measurable by theorem 11.
\end{proof}

\hrulefill\vspace{1em}

For the following, let $\cal C$ be the Cantor set,
Take $\varphi$ to be the Cantor-Lesbesgue function,
and $\psi(x)=\varphi(x)+x$, as in prop 21.
\begin{thm}[2.34]
	There exists a continuous, strictly increasing function on $[0,1]$
	that maps a set of positive measure onto a set of measure zero.
\end{thm}
\begin{proof}

	Since $\psi$ is strictly increasing,
	$\psi^{-1}$ is also strictly increasing.

	Note that $m(\psi(\cal C))=1$
	and $m(\psi^{-1}(\psi(\cal C)))=m(\cal C)=0$.
\end{proof}

\begin{thm}[2.36]
	If $(f-\varphi)|_{[0,1]\setminus\cal C}=0$
	and $f$ is continuous,
	then $f-\varphi=0$,
\end{thm}
\begin{proof}
	Let $(f-\varphi)|_{[0,1]\setminus\cal C}=0$,
	and take $x\in\cal C$.

	$\varphi(x)=\sup \varphi([0,x)\setminus\cal C)$.
	Suppose for some $\delta>0$, $f(x)=\varphi(x)+\delta$.
	Let $\varepsilon>0$ be arbitrary.
	Choose $x_1\in [0,x)\cap (\varphi(x)-\varepsilon,\varphi(x)\setminus\cal C)$.

	Since $f(x_1)=\varphi(x_1)\leq \varphi(x)=f(x)-\delta$,
	$f$ is discontinuous at $x$.
\end{proof}

\begin{rem}[2.37]
	No. Take $\psi$ and $\psi^{-1}$ as in prop 21.
\end{rem}

\begin{thm}[2.38]
	Let $f:[a,b]\rightarrow\bb R$ be Lipshitz,
	and $A\subseteq[a,b]$ be measurable.
	$f(A)$ is measurable.
\end{thm}
\begin{proof}[Sketch]
	Let $f:[a,b]\rightarrow\bb R$ be Lipshitz,
	with $|f(u)-f(v)|\leq c|u-v|$.
	If $A\subseteq[A,b]$ has measure zero,
	then for some $\varepsilon>0$,
	there is a collection of intervals $\{I_k\}$ such that
	\[ 0=m(A)\leq m\left(\bigcup_{k=1}^\infty I_k\right)<\frac \varepsilon c \]
	Now $f(A)\subseteq f(\bigcup_{k=1}^\infty I_k)$, so we have
	\begin{align*}
		m(A)&\leq m\left(\bigcup_{k=1}^\infty f(I_k)\right) \\
			&= \sum_{k=1}^\infty m(f(I_k)) \leq \sum_{k=1}^\infty c\cdot m(I_k) \\
			&<\frac {c\varepsilon} c = \varepsilon
	\end{align*}
	Since $\varepsilon$ was arbitrary, this shows $m(f(A))=0$.

	Let $F\subseteq[a,b]$ be $F_\sigma$.
	Since $f$ is continuous, $f(F)$ is $F_\sigma$.

	Now, let $E\subseteq[a,b]$ be measurable.
	There exists an $F_\sigma$ set $F$
	where $m^*(E\setminus F)=0$.

	Since $F\subseteq E$, $f(E)\setminus f(F)\subseteq f(E\setminus F)$.

	Therefore, we have $m^*(f(E)\setminus f(F))\leq m^*(f(E\setminus F))=0$, and $f(F)$ is $F_\sigma$.
	So $f(E)$ is measurable.
\end{proof}

\begin{thm}[2.39]
	Let $\cal C_\alpha$ be the generalized Cantor set.
	$\cal C_\alpha$ is closed,
	$[0,1]\setminus\cal C_\alpha$ is dense in $[0,1]$,
	and $m(\cal C_\alpha)=1-\alpha$.
\end{thm}
\begin{proof}
	Let $0 < \alpha < 1$
	Since $\cal C_\alpha$ is the intersection of closed sets, $\cal C_\alpha$ is closed.

	Let $0\leq a<b\leq 1$. Take $x=\frac{a+b}2$.
	If $x\in[0,1]\setminus\cal C_\alpha$, then this satisfies density.
	Otherwise, $x\in\cal C_\alpha$.
	At stage $k=\frac 1 {a-b}$, there is an interval $I$ removed within $\frac 1 {a-b}$ of $x$.
	If $y\in I$, then $a<y<b$.
	

	Since ${\cal C_\alpha}_k$ has length $1-\alpha+(\sfrac 2 3)^k$, $m(\cal C_\alpha)=1-\alpha$.
\end{proof}

\begin{rem}[2.40]
	Let $\cal C_\alpha$ be the generalized Cantor set.
	$[0,1]\setminus \cal C_\alpha$ has boundary $\cal C_\alpha$,
	since $\cal C_\alpha$ is closed
	and for every $\varepsilon >0$ and $x\in\cal C_\alpha$,
	$(x-\varepsilon,x+\varepsilon)\not\subseteq \cal C_\alpha$.
\end{rem}

\begin{thm}[2.41]
	The Cantor set is perfect.
\end{thm}
\begin{proof}[Sketch]
	Let $x\in\cal C,\varepsilon>0$.
	$x$ has a ternary expansion $a_1a_2a_3\cdots$ containing only the digits $\{0,2\}$.

	For all $n>2-\log_3 \varepsilon$,
	if $a_n=2$, let $b_n=0$. If $a_n=0$, let $b_n=2$.

	The ternary expansion $a_1a_2\cdots a_{n-1}b_na_{n+1}\cdots$
	will produce a new element $x'\in\cal C$,
	with $x'-x<\varepsilon$.
\end{proof}

\begin{thm}[2.42]
	Every perfect subset $X\subseteq\bb R$ is uncountable.
\end{thm}
\begin{proof}[Sketch]
	Let $X$ be countable and perfect.
	Enumerate $X=\{x_0,x_1,x_2\ldots\}$.
	Let $I_0=(x_0-1,x_0+1)$.

	For each $k$, take an open interval $I_k\ni x_k$
	such that $x_{k-1}\not\in\overline{I_k}\subseteq I_{k-1}$.
	Since $X$ is perfect, $x_k$ is a cluster point of $X$.

	Let $V = \bigcap_{k=1}^\infty \overline{I_k}\cap X$.
	Since $V$ is the intersection of descending 
	compact sets, $V$ is compact.
	By our construction, for each index $n$,
	$\bigcap_{k=1}^n \overline{I_k}\cap X$ is nonempty,
	since it contains $x_{n+1},x_{n+2},\ldots$.
	Therefore, $V$ is nonempty.

	However, for each index $k$, $x_k\not\in I_{k+1}$.
	So $x_k\not\in V$.
	But since $V$ is not empty, there is some $x'\in X$,
	$x'\neq x_k$.

\end{proof}

\begin{thm}[2.44]
	The Cantor set is nowhere dense in $\bb R$.
\end{thm}
\begin{proof}[Sketch]
	Let $C_k$ be the union of all intervals removed at step $k$
	in the construction of the Cantor set.
	Let $\cal O=(x-\varepsilon,x+\varepsilon)$ be open.
	Choose $n$ such that $\varepsilon < \sfrac 1 {3^{n-1}}$.

	Either $\cal O \subseteq C_k$ for $k<n$, or there is an interval
	$I=(x'-\sfrac 1 {3^n},x'+\sfrac 1 {3^n})$
	where $I\subseteq C_n$ and $I\subseteq\cal O$.

	Since $I\subseteq C_n$, $I\cap \cal C=\emptyset$.
	Therefore the Cantor set is nowhere dense.
\end{proof}

\begin{thm}[2.46]
	Let $f$ be continuous, and $B$ a Borel set.
	$f^{-1}(B)$ is Borel.
\end{thm}
\begin{proof}
	Let $f$ be continuous.
	Since for every open set $U$, $f^{-1}(U)$ is open,
	the inverse of any open set is Borel.

	Suppose $A,B$ are Borel sets and $f^{-1}(A)$ and $f^{-1}(B)$ are Borel.

	Since $f^{-1}(\bb R\setminus A)=\bb R\setminus f^{-1}(A)$,
	complements of Borel sets map back to Borel sets.

	Since $f^{-1}(A\cup B)=f^{-1}(A)\cup f^{-1}(B)$,
	unions of Borel sets map back to Borel sets.

	By induction, if $B$ is Borel, then $f^{-1}(B)$ is Borel.
\end{proof}

\begin{thm}[2.47]
	Let $f$ be continuous and strictly increasing.
	If $B$ is Borel, then $f(B)$ is Borel.
\end{thm}
\begin{proof}
	Since a continuous, strictly increasing function has a continuous inverse,
	$f(B)=(f^{-1})^{-1}(B)$ is Borel by 2.46.
\end{proof}
\end{document}
